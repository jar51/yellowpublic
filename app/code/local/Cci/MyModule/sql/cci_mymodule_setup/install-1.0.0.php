<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('cci_mymodule/developer'))
    ->addColumn('developer_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Developer ID')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
    ), 'Developer Name')
    ->addColumn('firstname', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
    ), 'Developer Firstname')
    ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT, 1000, array(
        'nullable'  => false,
    ), 'Developer Description')
    ->addColumn('xp_years', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
    ), 'Developer Xp Years')
    ->addColumn('hourly_rates', Varien_Db_Ddl_Table::TYPE_FLOAT, null, array(
        'nullable'  => false,
    ), 'Developer Hourly Rates');

$installer->getConnection()->createTable($table);

$installer->endSetup();