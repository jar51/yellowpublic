<?php

$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
        ->newTable($installer->getTable('cci_mymodule/location'))
        ->addColumn('bien_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'identity' => true,
            'nullable' => false,
            'primary' => true,
                ), 'bien ID')
        ->addColumn('nom_bien', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            'nullable' => false,
                ), 'nom du bien')
        ->addColumn('description', Varien_Db_Ddl_Table::TEXT, null, array(
            'nullable' => false,
                ), 'description du bien')
        ->addColumn('lien_images', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
                ), 'lien_images')
        ->addColumn('Type_Bien', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'nullable' => false,
            'default' => '1',
                ), 'Le type de bien : Apartement/Maison')
        ->addColumn('type_location', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'nullable' => false,
            'default' => '1',
                ), 'Le type : Location/colocation')
        ->addColumn('Nbre_pieces', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'nullable' => false,
                ), 'Nombre de pieces')
        ->addColumn('nbre_metre_carre', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            'nullable' => false,
                ), 'Nombre de mètres-carrés')
        ->addColumn('Lieu_ville', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            'nullable' => false,
                ), 'Lieu: situation geographique')
        ->addColumn('Lieu_commune', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            'nullable' => false,
                ), 'Lieu: situation geographique')
        ->addColumn('Lieu_code_postal', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            'nullable' => false,
                ), 'Lieu: situation geographique')
        ->addColumn('Lieu_departement', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            'nullable' => false,
                ), 'Lieu: situation geographique')
        ->addColumn('Lieu_region', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            'nullable' => false,
                ), 'Lieu: situation geographique')
        ->addColumn('date_debut', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
                ), 'date de début de période de location')
        ->addColumn('date_fin', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
                ), 'date de début de période de location')
        ->addColumn('disponible', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
    'nullable' => false,
    'default' => '1',
        ), 'disponible/ non disponible')
;

$installer->getConnection()->createTable($table);

$installer->endSetup();



