<?php 
class Magebees_Newproducts_Model_Observer
{
	public function prepareLayoutBefore(Varien_Event_Observer $observer) {
		$enabled = Mage::getStoreConfig('newproducts/general/enabled');
		$disable_output = Mage::getStoreConfig('advanced/modules_disable_output/Magebees_Newproducts');
		
		if (!$enabled) {
            return $this;
        }
		if ($disable_output) {
		    return $this;
        }

        $block = $observer->getEvent()->getBlock();

        if ("head" == $block->getNameInLayout()) {
           	$jquery_enabled = Mage::getStoreConfig('newproducts/general/jquery');
			$file = "newproducts/jquery.min_1.11.0.js";
			$path = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_JS). $file;
			if($path){
				if($jquery_enabled){
					$block->addJs($file);	
				}
			}
		}
		return $this;
    }
	
}
     
